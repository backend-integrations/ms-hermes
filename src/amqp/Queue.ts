
export const Themisto = 'Themisto';
export const Ganymede = 'Ganymede';

const redis = {
  port: process.env.REDIS_PORT || 6379, 
  host: process.env.REDIS_HOST || '127.0.0.1', 
  password: process.env.REDIS_PASS || 'localPass'
}

export const queues = {
  [Themisto]: {
    name: Themisto,
    hostId: process.env.REDIS_HOST,
    redis: redis
  },
  [Ganymede]: {
    name: Ganymede,
    hostId: process.env.REDIS_HOST,
    redis: redis
  }
};
